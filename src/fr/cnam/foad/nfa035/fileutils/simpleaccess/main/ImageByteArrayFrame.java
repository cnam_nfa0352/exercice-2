package fr.cnam.foad.nfa035.fileutils.simpleaccess.main;

import java.io.*;

public class ImageByteArrayFrame extends AbstractImageFrameMedia<ByteArrayOutputStream> {
    public ImageByteArrayFrame(ByteArrayOutputStream channel) {
        super(channel);
    }

    @Override
    public OutputStream getEncodedImageOutput() throws IOException {
        return this.getChannel();
    }

    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new ByteArrayInputStream(this.getChannel().toByteArray());
    }
}
