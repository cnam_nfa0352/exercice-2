package fr.cnam.foad.nfa035.fileutils.simpleaccess.main;

import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;

public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer<AbstractImageFrameMedia> {

    private OutputStream sourceOutputStream;

    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    public ImageDeserializerBase64StreamingImpl(OutputStream sourceOutputStream) {
        this.sourceOutputStream = sourceOutputStream;
    }

    @Override
    public InputStream getDeserializingStream(AbstractImageFrameMedia media) throws IOException {
        return new Base64InputStream(media.getEncodedImageInput());
    }
}
