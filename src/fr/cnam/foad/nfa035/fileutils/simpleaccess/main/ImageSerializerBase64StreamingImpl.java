package fr.cnam.foad.nfa035.fileutils.simpleaccess.main;

import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;

public class ImageSerializerBase64StreamingImpl implements ImageStreamingSerializer<File, AbstractImageFrameMedia>{

    @Override
    public void serialize(File source, AbstractImageFrameMedia media) throws IOException {
        try(OutputStream os = this.getSerializingStream(media)){
            this.getSourceInputStream(source).transferTo(os);
        }
    }

    @Override
    public InputStream getSourceInputStream(File source) throws FileNotFoundException {
        return new FileInputStream(source);
    }

    @Override
    public OutputStream getSerializingStream(AbstractImageFrameMedia media) throws IOException {
        return new Base64OutputStream(media.getEncodedImageOutput());
    }
}
