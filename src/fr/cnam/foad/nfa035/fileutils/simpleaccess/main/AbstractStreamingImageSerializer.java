package fr.cnam.foad.nfa035.fileutils.simpleaccess.main;

import java.io.IOException;
import java.io.OutputStream;


public abstract class AbstractStreamingImageSerializer<S,M> implements ImageStreamingSerializer<S,M> {


    @Override
    public final void serialize(S source, M media) throws IOException {
        try(OutputStream os = getSerializingStream(media)){
            getSourceInputStream(source).transferTo(os);
        }
    }

}
